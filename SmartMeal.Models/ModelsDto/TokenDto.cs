﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartMeal.Models.ModelsDto
{
    public class TokenDto : DtoBaseModel
    {
        public string Token { get; set; }
    }
}
