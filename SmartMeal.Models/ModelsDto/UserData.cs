﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartMeal.Models.ModelsDto
{
    public class UserData
    {
        public string Email { get; set; }

        public long? FacebookId { get; set; }
    }
}
