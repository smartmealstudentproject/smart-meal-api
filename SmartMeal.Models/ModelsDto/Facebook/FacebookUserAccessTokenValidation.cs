﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartMeal.Models.ModelsDto
{
    public class FacebookUserAccessTokenValidation
    {
        public FacebookUserAccessTokenData Data { get; set; }
    }
}
