﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SmartMeal.Models.BindingModels
{
    public class UserAuthBindingModel
    {
        public long Id { get; set; }

        public string Email { get; set; }
    }
}
